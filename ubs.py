import argparse
import re
import time
import requests
import random
from bs4 import BeautifulSoup
from PIL import Image
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

parser = argparse.ArgumentParser("A script to replace background image with a new highly rated UKClimbing photo")
parser.add_argument("-u", help="UKClimbing Username", type=str)
parser.add_argument("-p", help="UKClimbing Password", type=str)
parser.add_argument("-w", help="Minimum Width", type=int)
args = parser.parse_args()

USER = args.u
PASS = args.p
WIDTH = args.w

driver = webdriver.Firefox()

driver.get("https://www.ukclimbing.com/user")
username = WebDriverWait(driver, 20).until(
           EC.element_to_be_clickable(driver.find_element("id", "email")))
username.send_keys(USER)
password = WebDriverWait(driver, 20).until(
           EC.element_to_be_clickable(driver.find_element("id", "password")))
password.send_keys(PASS)
password.submit()

WebDriverWait(driver, 20).until(
        EC.presence_of_element_located(("id", "featured")))

def getdata(url):
    r = requests.get(url)
    return r.text

while True:
    photo_id = -1

    htmldata = getdata("https://www.ukclimbing.com/photos//?nstart={}&sort=v&category=12".format(random.randint(0, 1000)))
    soup = BeautifulSoup(htmldata, "html.parser")
    for item in soup.find_all("img", {'class':["img-fluid", "thumb_65345", "d-block", "mx-auto"]}):
        src = item["src"]
        if "img.ukclimbing.com" in src and "ads" not in src:
            im = Image.open(requests.get(src, stream=True).raw)
            width, height = im.size
            aspect = width / height
            if height / width == 0.5625:
                photo_id = re.search("(?<=\\/)(\\d+)(?=\\?)", src)[0]
                break

    if photo_id == -1:
        continue

    url = "https://www.ukclimbing.com/photos/dbpage.php?id={}".format(photo_id)
    driver.get(url)

    htmldata = driver.page_source

    soup = BeautifulSoup(htmldata, "html.parser")
    src = soup.find("a", { "class": ["photoswipe"],  "href": True })["href"]
    im = Image.open(requests.get(src, stream=True).raw)
    width, _ = im.size
    if width >= WIDTH:
        im.save("/home/hunter/.config/sway/wallpaper.png", "PNG")
        break

driver.quit()
